"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const PORT = process.env.PORT || 3001;
const app = (0, express_1.default)();
app.use(express_1.default.static("public"));
app.use(express_1.default.urlencoded({ extended: true }));
const todos = [];
app.post("/todo", (req, res) => {
    console.log("recieved", req.body);
    todos.push(req.body.todo);
    res.send(insertTodos(todos));
});
app.listen(PORT, () => console.log(`Server is up and running on Port:${PORT}`)); //eslint-disable-line
function insertTodos(todos) {
    return `<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/output.css">
  <title>Document</title>
</head>

<body>
  <div><ul>${todos.reduce((acc, todo) => acc + `<li>${todo}</li>`, "")}</ul></div>
  <form method="post" action="/todo" enctype="application/x-www-form-urlencoded">
    <input name="todo" type="text" />
    <button type="submit">Save todo</button>
  </form>
</body>

</html>`;
}
