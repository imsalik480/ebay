"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postRelation = exports.posts = exports.userRelation = exports.users = void 0;
const drizzle_orm_1 = require("drizzle-orm");
const pg_core_1 = require("drizzle-orm/pg-core");
exports.users = (0, pg_core_1.pgTable)('users', {
    id: (0, pg_core_1.uuid)('id').primaryKey().defaultRandom(),
    username: (0, pg_core_1.varchar)("username").notNull(),
});
exports.userRelation = (0, drizzle_orm_1.relations)(exports.users, ({ many }) => ({
    posts: many(exports.posts),
}));
exports.posts = (0, pg_core_1.pgTable)('posts', {
    id: (0, pg_core_1.uuid)('id').primaryKey().defaultRandom(),
    title: (0, pg_core_1.varchar)("title").notNull(),
    content: (0, pg_core_1.varchar)("content").notNull(),
    userId: (0, pg_core_1.uuid)("author_id").notNull(),
});
exports.postRelation = (0, drizzle_orm_1.relations)(exports.posts, ({ one }) => ({
    author: one(exports.users, {
        fields: [exports.posts.userId],
        references: [exports.users.id],
    })
}));
