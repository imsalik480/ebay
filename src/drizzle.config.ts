
import type { Config } from "drizzle-kit";

export default {
	schema: "./src/db/schema.ts",
	driver: "pg",
	dbCredentials: {
		connectionString: "postgres://pcwin385:ZNjwJAH21sfb@ep-dark-flower-87699376.ap-southeast-1.aws.neon.tech/neondb?sslmode=require",
	},
	out: "./drizzle",
} satisfies Config;
