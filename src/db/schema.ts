import { relations } from 'drizzle-orm';
import { uuid, pgTable, varchar } from 'drizzle-orm/pg-core';

export const users = pgTable('users', {
  id: uuid('id').primaryKey().defaultRandom(),
  username: varchar("username").notNull(),
});

export const userRelation = relations(users, ({ many }) => ({
  posts: many(posts),
}))

export const posts = pgTable('posts', {
  id: uuid('id').primaryKey().defaultRandom(),
  title: varchar("title").notNull(),
  content: varchar("content").notNull(),
  userId: uuid("author_id").notNull(),
});

export const postRelation = relations(posts, ({ one }) => ({
  author: one(users, {
    fields: [posts.userId],
    references: [users.id],
  })
}))