import { drizzle, PostgresJsDatabase } from 'drizzle-orm/postgres-js';
import postgres from 'postgres';
import * as schema from './schema';

const queryClient = postgres("postgres://pcwin385:ZNjwJAH21sfb@ep-dark-flower-87699376.ap-southeast-1.aws.neon.tech/neondb?sslmode=require", { ssl: true });
export const db = drizzle(queryClient, { schema });
